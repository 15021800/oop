package people;
import java.util.Date;

/**
 * Created by MyPC on 13/10/2016.
 */
public class Employee extends Person {
    private double salary;

    public Employee(String name, Date birthday, double salary) {
        super(name, birthday);

        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return String.format("%s %.2f", super.toString(), this.salary);
    }
}
