package people;

/**
 * Created by MyPC on 13/10/2016.
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person {
    private String name;
    private Date birthday;

    Person(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String date = (format.format(this.birthday));

        return(this.name + " " + date);
    }
}
