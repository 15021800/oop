import people.*;
import java.util.Date;

/**
 * Created by MyPC on 13/10/2016.
 */
public class Manager extends Employee {
    private Employee assistant;

    Manager(String name, Date birthday, double salary) {
        super(name, birthday, salary);
    }

    public void setAssistant(Employee assistant) {
        this.assistant = assistant;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
