/**
 * Created by MyPC on 13/10/2016.
 */
public class Gambler extends BankAccount {
    Gambler(int balance) {
        super(balance);
    }

    @Override
    public boolean withdraw(int money) {
        int random = (int) (Math.random()*100);

        if (random < 50) {
            return super.withdraw(0);
        } else {
            return super.withdraw(money*2);
        }
    }

    public int endMonthCharge() {
        return 0;
    }
}
