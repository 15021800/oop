/**
 * Created by MyPC on 13/10/2016.
 */
public class BankAccountTest {
    public static void main(String[] args) {
        FlatFee acc1 = new FlatFee(10000);
        NickleNDime acc2 = new NickleNDime(2000);
        Gambler acc3 = new Gambler(0);

        acc1.deposit(10000);
        System.out.println(acc1.endMonthCharge());
        acc1.withdraw(-1);
        acc1.endMonth();

        acc2.deposit(10000);
        acc2.withdraw(-1);
        acc2.withdraw(10000);
        System.out.println(acc2.endMonthCharge());
        acc2.endMonth();

        acc3.deposit(-1);
        System.out.println(acc3.endMonthCharge());
        acc3.withdraw(-1);
        System.out.printf("%b %b %b \n", acc3.withdraw(10000), acc3.withdraw(10000), acc3.withdraw(10000));
        acc3.endMonth();
    }
}
