package oop.util;

import junit.framework.TestCase;

/**
 * Created by MyPC on 06/10/2016.
 */
public class StackTest extends TestCase {
    public void testPushNTop() throws Exception {
        Stack stack = new Stack();
        stack.push("hello");

        assertEquals("hello", stack.top());
    }

    public void testPop() throws Exception {
        Stack stack = new Stack();
        stack.push("Hello");
        stack.push("Im Quang");

        assertEquals("Im Quang", stack.pop());
        assertEquals("Hello", stack.pop());
    }

    public void testIsEmpty() throws Exception {
        Stack stack = new Stack();

        stack.push("Nguyen Anh Quang");
        assertEquals(false, stack.isEmpty());

        stack.pop();
        assertEquals(true, stack.isEmpty());
    }

}