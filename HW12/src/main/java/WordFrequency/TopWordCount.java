package WordFrequency;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by MyPC on 09/12/2016.
 */
public class TopWordCount {
    public static void main(String[] args) throws FileNotFoundException {
        FileInputStream input = new FileInputStream("input.txt");
        Scanner scanner = new Scanner(input);

        Map<String, Integer> list = new HashMap<String, Integer>();
        int maxValue = -1;
        String topWord = "";

        int L = Integer.parseInt(args[0]);
        while (scanner.hasNext()) {
            String key = scanner.next();

            if (key.length() < L ) continue;
            if (!list.isEmpty()) {
                int value = list.get(key);
                ++value;
                list.put(key, value);

                if (value > maxValue) {
                    maxValue = value;
                    topWord = key;
                }
            }
        }

        System.out.println(topWord + " " + maxValue);
    }
}
