package pasture;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MyPC on 16/11/2016.
 */

public class grassTufts extends Plants {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/plant-transparent.gif"));

    public grassTufts(Pasture pasture, int reproduceDelay) {
        super(pasture, reproduceDelay);
    }

    @Override
    protected void Reproduce() {
        if (livedTime % reproduceDelay == 0) {
            Point neighbour =
                    (Point)getRandomMember
                            (pasture.getFreeNeighbours(this));

            if(neighbour != null)
                pasture.addEntity(new grassTufts(this.pasture, this.reproduceDelay), neighbour);
        }
    }

    @Override
    public ImageIcon getImage() {
        return image;
    }
}
