package pasture;

import java.awt.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by MyPC on 16/11/2016.
 */
public abstract class Animals extends LiveObjects {
    protected int moveDelay;
    protected int reproduceDelay;
    protected int starveTime;
    protected int starveDelay;
    protected int reproduceAge;
    protected int sight;
    protected int lastReproduce = 0;
    protected Pasture pasture;
    protected boolean hadEaten = false;

    public Animals(Pasture pasture, int reproduceDelay, int reproduceAge, int moveDelay, int starveTime, int sight) {
        this.pasture = pasture;
        this.moveDelay =  moveDelay;
        this.reproduceDelay = reproduceDelay;
        this.starveTime = starveTime;
        this.reproduceAge = reproduceAge;
        this.sight = sight;

        starveDelay = starveTime;
    }

    abstract protected boolean isEnemy(Entity entity);
    abstract protected boolean isFood(Entity entity);
    abstract protected void eat();

    protected int getPointWeight(Point point, Point thisPos) {
        int enemyDis = sight*2 + 2;
        int foodDis = sight*2 + 2;
        int weight = 0;
        if (point.equals(thisPos)) weight -= 100;

        //System.out.println(point.x + " " + point.y + "point");
        for (int z = 1; z <= sight + 1; ++z) {
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    Point p = new Point(point.x + x*z,
                            point.y + y*z);


                    //System.out.println(p.x + " " + p.y + " ");

                    if ((p.x > 0) && (p.y > 0) && (p.x < pasture.width) && (p.y < pasture.height) && (Math.abs(p.x - thisPos.x) <= sight) && (Math.abs(p.y - thisPos.y) <= sight)) {
                        Collection<Entity> l = pasture.getEntitiesAt(p);

                        if (l != null) {
                            int newDis = Math.max(Math.abs(x * z), Math.abs(y * z));
                            for (Entity e : l) {
                                if (isFood(e) && (foodDis > newDis)) {
                                    foodDis = newDis;
                                    //System.out.println(p.x + " " + p.y + " " + x*z + " " + y*z + " " + point.x + " " + point.y + " " + foodDis);
                                }

                                if (isEnemy(e) && (enemyDis > newDis)) {
                                    enemyDis = newDis;
                                }
                            }
                        }
                    }
                }
            }
        }

        weight += enemyDis*100 - foodDis;
        return weight;
    }

    protected Collection<Point> calNextMove(Collection<Point> points) {
        int bestMove = -1000000000;
        Point thisPos = pasture.getPosition(this);
        points.add(thisPos);
        Set<Point> moveSet = new HashSet<Point>();

        //System.out.println(" " + thisPos.x + " " + thisPos.y + " " + this.moveDelay + " " + this.livedTime);
        for (Point p: points) {
            int weight = getPointWeight(p, thisPos);
            //System.out.println(weight + " " + p.x + " " + p.y);

            if (weight > bestMove) {
                bestMove = weight;
               //System.out.println(weight + " " + p.x + " " + p.y);

                moveSet.clear();
                moveSet.add(p);
            } else if (weight == bestMove) {
                moveSet.add(p);
            }
        }

        //System.out.println(bestMove + " " + tmpP.x + " " + tmpP.y);
        return moveSet;
    }

    protected void move() {
        if (livedTime % moveDelay == 0) {
            Collection<Point> freePoints = pasture.getFreeNeighbours(this);

            Point neighbour =
                    (Point)getRandomMember
                            (this.calNextMove(freePoints));

            if(neighbour != null)
                pasture.moveEntity(this, neighbour);
        }
    }

    @Override
    public void tick() {
        super.tick();
        if (!alive) return;

        --starveDelay;

        if (starveDelay == 0) {
            pasture.removeEntity(this);
            return;
        }

        move();
        if (hadEaten) this.Reproduce();
        this.eat();
    }
}
