package pasture;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Created by MyPC on 16/11/2016.
 */
public class Sheep extends Animals {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/sheep-transparent.gif"));

    public Sheep(Pasture pasture, int reproduceDelay, int reproduceAge, int moveDelay, int starveTime, int sight) {
        super(pasture, reproduceDelay, reproduceAge, moveDelay, starveTime, sight);
    }

    @Override
    protected boolean isEnemy(Entity entity) {
        String name = entity.getClass().getName();
        return (name.equals("pasture.Wolves") || name.equals("pasture.Fences"));
    }

    @Override
    protected boolean isFood(Entity entity) {
        String name = entity.getClass().getName();
        return name.equals("pasture.grassTufts");
    }

    @Override
    protected void eat() {
        Point p = pasture.getPosition(this);
        Collection<Entity> l = pasture.getEntitiesAt(p);

        for (Entity e: l) {
            if (isFood(e)) {
                grassTufts tmp = (grassTufts) e;
                tmp.setDead();
                pasture.removeEntity(e);

                starveDelay = starveTime;
                this.hadEaten = true;
                break;
            }
        }
    }

    @Override
    protected void Reproduce() {
        if ((livedTime >= reproduceAge) && ((livedTime - lastReproduce) > reproduceDelay)) {
            Point neighbour =
                    (Point)getRandomMember
                            (pasture.getFreeNeighbours(this));

            if(neighbour != null)
                pasture.addEntity(new Sheep(this.pasture, this.reproduceDelay, this.reproduceAge, this.moveDelay, this.starveTime, this.sight), neighbour);

            lastReproduce = livedTime;
        }
    }

    @Override
    public ImageIcon getImage() {
        return image;
    }
}
