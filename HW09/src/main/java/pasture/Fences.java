package pasture;

import javax.swing.*;

/**
 * Created by MyPC on 16/11/2016.
 */
public class Fences implements Entity {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/fence.gif"));

    @Override
    public void tick() {
    }

    @Override
    public ImageIcon getImage() {
        return image;
    }

    @Override
    public boolean isCompatible(Entity otherEntity) {
        return false;
    }
}
