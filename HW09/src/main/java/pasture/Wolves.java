package pasture;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Created by Admin on 11/16/2016.
 */
public class Wolves extends Animals {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/wolf-transparent.gif"));

    public Wolves(Pasture pasture, int reproduceDelay, int reproduceAge, int moveDelay, int starveTime, int sight) {
        super(pasture, reproduceDelay, reproduceAge, moveDelay, starveTime, sight);
    }

    @Override
    protected boolean isEnemy(Entity entity) {
        return false;
    }

    @Override
    protected boolean isFood(Entity entity) {
        String name = entity.getClass().getName();
        return name.equals("pasture.Sheep");
    }

    @Override
    protected void eat() {
        Point p = pasture.getPosition(this);
        Collection<Entity> l = pasture.getEntitiesAt(p);

        for (Entity e: l) {
            if (isFood(e)) {
                Sheep tmp = (Sheep) e;
                tmp.setDead();
                pasture.removeEntity(e);

                starveDelay = starveTime;
                this.hadEaten = true;
                break;
            }
        }
    }

    @Override
    protected void Reproduce() {
        if ((livedTime >= reproduceAge) && ((livedTime - lastReproduce) > reproduceDelay)) {
            Point neighbour =
                    (Point)getRandomMember
                            (pasture.getFreeNeighbours(this));

            if(neighbour != null)
                pasture.addEntity(new Wolves(this.pasture, this.reproduceDelay, this. reproduceAge, this.moveDelay, this.starveTime, this.sight), neighbour);

            lastReproduce = livedTime;
        }
    }

    @Override
    public ImageIcon getImage() {
        return image;
    }
}
