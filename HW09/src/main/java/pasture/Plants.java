package pasture;

/**
 * Created by MyPC on 16/11/2016.
 */
public abstract class Plants extends LiveObjects {
    protected Pasture pasture;
    protected int reproduceDelay;

    public Plants(Pasture pasture, int reproduceDelay) {
        this.pasture = pasture;
        this.reproduceDelay = reproduceDelay;
    }


    @Override
    public void tick() {
        if (!alive) return;

        super.tick();
        this.Reproduce();
    }
}
