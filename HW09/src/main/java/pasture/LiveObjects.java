package pasture;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by MyPC on 16/11/2016.
 */
public abstract class LiveObjects implements Entity {
    protected int livedTime = 0;
    protected boolean alive = true;

    abstract protected void Reproduce();

    public void setDead() {
        alive = false;
    }

    @Override
    public void tick() {
        ++livedTime;
    }


    @Override
    public boolean isCompatible(Entity otherEntity) {
        String otherName = otherEntity.getClass().getName();
        String thisName = this.getClass().getName();

        return !otherName.equals("pasture.Fences") && !thisName.equals(otherName);
    }

    protected static <X> X getRandomMember(Collection<X> c) {
        if (c.size() == 0)
            return null;

        Iterator<X> it = c.iterator();
        int n = (int)(Math.random() * c.size());

        while (n-- > 0) {
            it.next();
        }

        return it.next();
    }
}
