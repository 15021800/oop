package bankAccount;

/**
 * Created by MyPC on 13/10/2016.
 */
public class NickleNDime extends BankAccount {
    private int fee = 0;

    NickleNDime(int balance) throws InvalidAmountException {
        super(balance);
    }

    @Override
    public void withdraw(int money) throws InvalidAmountException, OverdrawException {
        super.withdraw(money);

        fee += 2000;
    }

    public int endMonthCharge() {
        return fee;
    }
}
