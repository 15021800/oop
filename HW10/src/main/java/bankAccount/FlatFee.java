package bankAccount;

/**
 * Created by MyPC on 13/10/2016.
 */
public class FlatFee extends BankAccount {
    private int fee = 10000;

    FlatFee(int balance) throws InvalidAmountException {
        super(balance);
    }

    @Override
    public int endMonthCharge() {
        return fee;
    }
}
