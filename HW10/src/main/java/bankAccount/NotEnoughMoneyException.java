package bankAccount;

/**
 * Created by Admin on 11/24/2016.
 */
public class NotEnoughMoneyException extends Throwable {
    public NotEnoughMoneyException(String name) {
        super(name);
    }

    public NotEnoughMoneyException() {
        this("not enough money to pay end-month-charge");
    }
}
