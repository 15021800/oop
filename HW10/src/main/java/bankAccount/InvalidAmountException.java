package bankAccount;

/**
 * Created by Admin on 11/24/2016.
 */
public class InvalidAmountException extends Throwable {
    public InvalidAmountException(String name) {
        super(name);
    }

    public InvalidAmountException() {
        this("Amount is negative or zero");
    }
}
