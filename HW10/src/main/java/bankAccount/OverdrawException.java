package bankAccount;

/**
 * Created by Admin on 11/24/2016.
 */
public class OverdrawException extends Throwable {
    public OverdrawException(String name) {
        super(name);
    }

    public OverdrawException() {
        this("withdraw more than allowed");
    }
}
