package bankAccount;

/**
 * Created by MyPC on 13/10/2016.
 */
public class Gambler extends BankAccount {
    Gambler(int balance) throws InvalidAmountException {
        super(balance);
    }

    @Override
    public void withdraw(int money) throws InvalidAmountException, OverdrawException {
        int random = (int) (Math.random()*100);

        if (random >= 50) {
            super.withdraw(money*2);
        }
    }

    public int endMonthCharge() {
        return 0;
    }
}
