package bankAccount;

abstract class BankAccount {
    private int balance;
    private int numberOfTransactions = 0;

    BankAccount(int balance) throws InvalidAmountException {
        if (balance <= 0) throw new InvalidAmountException();
        this.balance = balance;
    }

    public void deposit(int money) throws InvalidAmountException {
        if (money <= 0) throw new InvalidAmountException();

        ++numberOfTransactions;
        balance += money;
    }

    public void withdraw(int money) throws InvalidAmountException, OverdrawException {
        if (money <= 0) throw new InvalidAmountException();
        if (money > balance) throw new OverdrawException();

        ++numberOfTransactions;
        balance -= money;
    }

    abstract public int endMonthCharge();

    public void endMonth() throws NotEnoughMoneyException {
        int charge = endMonthCharge();

        if (charge > balance) throw new NotEnoughMoneyException();

        balance -= charge;
        System.out.printf("%d %d %d\n", balance, numberOfTransactions, charge);
        numberOfTransactions = 0;
    }
}
