package bigInteger;

/**
 * Created by Admin on 11/24/2016.
 */
public class ExceptionNotANumber extends ExceptionInInitializerError {
    public ExceptionNotANumber() {
        this("Value is not a number ");
    }

    public ExceptionNotANumber(String name) {
        super(name + "- Init string is not a number ");
    }
}
