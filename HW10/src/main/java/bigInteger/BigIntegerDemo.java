package bigInteger;

/**
 * Created by MyPC on 06/10/2016.
 */
public class BigIntegerDemo {
    public static void main(String[] args) {
        BigInteger bigInt1 = new BigInteger(305L);
        BigInteger bigInt2 = new BigInteger("000000000000000001997");
        BigInteger bigInt3 = bigInt1.clone();

        BigInteger res1 = bigInt1.subtract(bigInt2);
        BigInteger res2 = bigInt2.add(bigInt3);

        System.out.printf("Result 1: %d \n\n", res1.toLong());
        System.out.println("Result 2: " + res2.toString());
    }
}
