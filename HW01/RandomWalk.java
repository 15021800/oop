public class RandomWalk {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        StdDraw.setXscale(-n, +n);
        StdDraw.setYscale(-n, +n);
        StdDraw.clear(StdDraw.GRAY);
        StdDraw.enableDoubleBuffering();

        int dr[][] = {{0, 1, 0, -1},
                        {1, 0, -1, 0}};
        int x = 0, y = 0;
        int steps = 0;
        int direct = 0, turn = 0, walked = 0;
        while (Math.abs(x) < n && Math.abs(y) < n) {
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledSquare(x, y, 0.45);

            x = x + dr[0][direct];
            y = y + dr[1][direct];

            walked++;
            if (walked > turn) {
                walked = 0;
                direct = (direct + 1) % 4;
                if (direct % 2 == 0) ++turn;
            }

            steps++;
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.filledSquare(x, y, 0.45);
            StdDraw.show();
            StdDraw.pause(40);
        }
        StdOut.println("Total steps = " + steps);
    }

}
