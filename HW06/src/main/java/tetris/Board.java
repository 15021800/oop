// Board.java
package tetris;
/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private int maxHeight = 0;
	private int maxHeightBackup;
	private int[] widths;
	private int[] heights;
	private int[] widthsBackup;
	private int[] heightsBackup;
	private boolean[][] grid;
	private boolean[][] backup;
	private boolean DEBUG = true;
	boolean committed;
	
	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		widths = new int[height];
		heights = new int[width];
		backup = new boolean[width][height];
		widthsBackup = new int[height];
		heightsBackup = new int[width];
		committed = true;

		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {
		return maxHeight; // YOUR CODE HERE
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
		if (DEBUG) {
			int[] colCheck = new int[width];
			int maxHeightCheck = 0;

			for (int i = 0; i < height; ++i) {
				int rowCheck = 0;
				for (int j = 0; j < width; ++j) {
					if (grid[j][i]) {
						++rowCheck;
						colCheck[j] = Math.max(colCheck[j], i + 1);
					}
				}
				if (rowCheck != widths[i]) throw new RuntimeException(String.format("widths[] is not sanity, grid: %d - widths[%d]: %d", rowCheck, i, widths[i]));
			}

			for (int i = 0; i < width; ++i) {
				if (colCheck[i] != heights[i]) throw new RuntimeException(String.format("heights[] is not sanity, grid: %d - heights[%d]: %d", colCheck[i], i, heights[i]));
				maxHeightCheck = Math.max(maxHeightCheck, colCheck[i]);
			}

			if (maxHeightCheck != maxHeight) throw new RuntimeException(String.format("maxHeight is not sanity, grid: %d - maxHeight: %d", maxHeightCheck, maxHeight));
			// YOUR CODE HERE
		}
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	*/
	public int dropHeight(Piece piece, int x) {
		int[] Skirt = piece.getSkirt();
		int Row = heights[x] - Skirt[0];
		int base = Skirt[0];

		for (int i = 1; i < Skirt.length; ++i) {
			if (Row < heights[x + i] + (base - Skirt[i])) {
				Row = heights[x + i] - Skirt[i];
				base = Skirt[i];
			}
		}

		return(Row);
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	*/
	public int getColumnHeight(int x) {
		return heights[x];
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		return widths[y];
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
		if ((x >= width) || (y >= height) || (x < 0) || (y < 0) || (grid[x][y])) return true;
		return false; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");

		Backup();
		committed = false;
		int result = PLACE_OK;

		TPoint pieceBody[] = piece.getBody();
		for (TPoint point: pieceBody) {
			int i = x + point.x;
			int j = y + point.y;

			if ((i > width - 1) || (j > height - 1) || (i < 0) || (j < 0)) {
				result = PLACE_OUT_BOUNDS;
				return result;
			}

			if (grid[i][j]) {
				result = PLACE_BAD;
				return result;
			}
		}

		for (TPoint point: pieceBody) {
			int i = x + point.x;
			int j = y + point.y;

			grid[i][j] = true;

			/////////////////////////////////////////
			heights[i] = Math.max(heights[i], j + 1);
			maxHeight = Math.max(maxHeight, heights[i]);
			widths[j]++;
			if (getRowWidth(j) == width) result = PLACE_ROW_FILLED;
		}
		// YOUR CODE HERE

		sanityCheck();
		return result;
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
	public int clearRows() {
		int rowsCleared = 0;
		Backup();
		committed = false;

		heights = new int[width];
		int length = maxHeight;
		maxHeight = 0;

		for (int i = 0; i < length; ++i) {
			if (widths[i] == width) {
				++rowsCleared;
				continue;
			}

			for (int j = 0; j < width; ++j) {
				grid[j][i - rowsCleared] = grid[j][i];

				if (grid[j][i]) {
					heights[j] = Math.max(heights[j], i - rowsCleared + 1);
					maxHeight = Math.max(maxHeight, i - rowsCleared + 1);
				}
			}
			widths[i - rowsCleared] = widths[i];
		}

		for (int i = length - rowsCleared; i < length; ++i) {
			widths[i] = 0;
			for (int j = 0; j < width; ++j) {
				grid[j][i] = false;
			}
		}
		// YOUR CODE HERE
		sanityCheck();
		return rowsCleared;
	}

	private void Backup() {
		if (committed) {
			for (int i = 0; i < width; ++i) {
				System.arraycopy(grid[i], 0, backup[i], 0, height);
			}
			System.arraycopy(heights, 0, heightsBackup, 0, width);
			System.arraycopy(widths, 0, widthsBackup, 0, height);
			maxHeightBackup = maxHeight;
		}
	}

	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
	public void undo() {
		if (!committed) {
			for (int i = 0; i < width; ++i) {
				System.arraycopy(backup[i], 0, grid[i], 0, height);
			}

			System.arraycopy(heightsBackup, 0, heights, 0, width);
			System.arraycopy(widthsBackup, 0, widths, 0, height);
			maxHeight = maxHeightBackup;

			commit();
			sanityCheck();
		}
		// YOUR CODE HERE
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}


