package tetris;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MyPC on 10/11/2016.
 */
public class JBrainTetris extends JTetris {
    private Brain brain;
    private JCheckBox brainMode;
    private JSlider adversary;
    private JLabel OK;
    /**
     * Creates a new JTetris where each tetris square
     * is drawn with the given number of pixels.
     *
     * @param pixels
     */
    JBrainTetris(int pixels) {
        super(pixels);
        brain = new DefaultBrain();
    }

    @Override
    public void tick(int verb) {
        if (brainMode.isSelected()) {
            super.board.undo();

            int count = 0;
            Brain.Move move = null;

            if (count != super.count) {
                count = super.count;

                move = brain.bestMove(super.board, super.currentPiece, super.board.getHeight() - 4, null);
            }

            if ((count == super.count) && (verb == DOWN) && (move != null)) {
                if (move.x > super.currentX) tick(RIGHT);
                else if (move.x < super.currentX) tick(LEFT);

                if (!move.piece.equals(super.currentPiece))
                    super.currentPiece = super.currentPiece.fastRotation();
            }
        }

        super.tick(verb);
    }

    @Override
    public Piece pickNextPiece() {
        int rand = (int) (Math.random()*99 + 1);
        if (rand >= adversary.getValue()) {
            OK.setText("ok");
            return super.pickNextPiece();
        } else {
            Piece worstPiece = null;
            Brain.Move move;
            double worstScore = 0;

            for (Piece p: super.pieces) {
                move = brain.bestMove(super.board, p, super.board.getHeight() - 4, null);

                if (move == null) {
                    worstPiece = p;
                    break;
                } else {
                    if (move.score > worstScore) {
                        worstScore = move.score;
                        worstPiece = p;
                    }
                }
            }

            OK.setText("*ok*");
            return worstPiece;
        }
    }

    @Override
    public JComponent createControlPanel() {
        JComponent panel = super.createControlPanel();

        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        panel.add(brainMode);

        // make a little panel, put a JSlider in it. JSlider responds to getValue()
        JPanel little = new JPanel();
        little.add(new JLabel("Adversary:"));
        adversary = new JSlider(0, 100, 0); // min, max, current
        adversary.setPreferredSize(new Dimension(100,15));
        little.add(adversary);
        // now add little to panel of controls
        OK = new JLabel("ok");
        panel.add(OK);
        panel.add(little);

        return panel;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) { }

        JBrainTetris tetris = new JBrainTetris(16);
        JFrame frame = JBrainTetris.createFrame(tetris);
        frame.setVisible(true);
    }
}
