public class BigInteger
{
    private String number = "";
    private String negative = "";

    public BigInteger(long init) {
        number = String.valueOf(init);
    }

    public BigInteger(String init) {
        this.number = init;
        int pos = 0;
        negative = "";

        if (this.checkNegative()) {
            pos = 1;
            negative = "-";
        }
        while ((pos < init.length() - 1) && (init.charAt(pos) == '0')) ++pos;

        this.number = negative + this.number.substring(pos);
    }

    public String toString() {
        return number;
    }

    public boolean equals(Object other) {
        if (other instanceof  BigInteger) {
            BigInteger obj = (BigInteger) other;

            if (this.number.equals(obj.number)) return true;

        }

        return  false;
    }

    public long toLong() {
        long l = Long.parseLong(number);
        return l;
    }

    private void makeLengthEqual(BigInteger other) {
        while (this.number.length() < other.number.length()) this.number = "0" + this.number;
        while (this.number.length() > other.number.length()) other.number = "0" + other.number;

    }

    private BigInteger choseFormat(BigInteger other, char Formula) {
        negative = "";
        BigInteger newNum = new BigInteger(0);

        if ((this.checkNegative()) && (other.checkNegative())) {
            negative = "-";
            this.number = this.number.substring(1);
            other.number = other.number.substring(1);

            if (Formula == '+') {
                newNum = this.sum(other);
                newNum.number = negative + newNum.number;
            } else {
                newNum = other.minus(this);
            }

            this.number = negative + this.number;
            other.number = negative + other.number;
        } else
        if (this.checkNegative()) {
            negative = "-";
            this.number = this.number.substring(1);

            if (Formula == '+') {
                newNum = other.minus(this);
            } else {
                newNum = this.sum(other);
                newNum.number = negative + newNum.number;
            }

            this.number = negative + this.number;
        } else
        if (other.checkNegative()) {
            negative = "-";
            other.number = other.number.substring(1);

            if (Formula == '+') {
                newNum = this.minus(other);
            } else {
                newNum = this.sum(other);
            }

            other.number = negative + other.number;
        } else {
            if (Formula == '+') {
                newNum = this.sum(other);
            } else {
                newNum = this.minus(other);
            }
        }

        return newNum;
    }

    public BigInteger add(BigInteger other) {
        return this.choseFormat(other, '+');
    }

    private BigInteger sum(BigInteger other) {
        makeLengthEqual(other);

        BigInteger res = new BigInteger(0);
        res.number = "";
        int save = 0;

        for (int i = this.number.length() - 1; i >= 0; --i) {
            char x = this.number.charAt(i);
            char y = other.number.charAt(i);

            int miniSum = (x + y) - '0' - '0';
            miniSum += save;


            res.number = (char)((miniSum % 10) + '0') + res.number;
            save = miniSum / 10;
        }

        if (save > 0) res.number = '1' + res.number;
        return res;
    }

    public BigInteger subtract(BigInteger other) {
        return this.choseFormat(other, '-');
    }

    private BigInteger minus(BigInteger other) {
        makeLengthEqual(other);
        String num1, num2;
        boolean negative = false;

        if (this.compareTo(other) == -1) {
            num1 = other.number; //new String(other.number);
            num2 = this.number; //new String(this.number);
            negative = true;
        } else {
            num1 = this.number; //new String(this.number);
            num2 = other.number; //new String(other.number);
        }

        BigInteger res = new BigInteger("0");
        res.number = "";
        int save = 0;

        for (int i = num1.length() - 1; i >= 0; --i) {
            char x = num1.charAt(i);
            char y = num2.charAt(i);

            int miniSubtract = (x - y);
            miniSubtract += save;
            if (miniSubtract < 0) {
                save = -1;
                miniSubtract += 10;
            } else save = 0;

            res.number = (char)(miniSubtract + '0') + res.number;
        }

        if (negative) res.number = '-' + res.number;
        return res;
    }

    public int compareTo(BigInteger other) {
        if (this.equals(other)) return 0;

        if (this.number.length() < other.number.length()) return -1;
        if (this.number.length() > other.number.length()) return 1;

        for (int i = 0; i < this.number.length(); ++i) {
            if (this.number.charAt(i) < other.number.charAt(i)) return -1;
            if (this.number.charAt(i) > other.number.charAt(i)) return  1;
        }

        return 0;
    }

    public BigInteger clone() {
        BigInteger newNumber = new BigInteger(number);

        return newNumber;
    }

    private boolean checkNegative() {
        return(this.number.charAt(0) == '-');
    }
}