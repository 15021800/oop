package expressions;

public interface Expression {
    @Override
    String toString();
    int evaluate();
}
