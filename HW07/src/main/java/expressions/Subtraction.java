package expressions;

public class Subtraction implements BinaryExpression {
    private Expression left;
    private Expression right;

    public Subtraction(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public Expression left() {
        return this.left;
    }

    public Expression right() {
        return this.right;
    }

    public String toString() {
        return "(" + left.toString() + " - " + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate() - right.evaluate();
    }
}
