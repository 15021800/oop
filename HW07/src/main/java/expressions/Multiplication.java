package expressions;

public class Multiplication implements BinaryExpression {
    private Expression left;
    private Expression right;

    public Multiplication(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    public String toString() {
        return "(" + left.toString() + " * " + right.toString() + ")";
    }

    public int evaluate() {
        return left.evaluate()*right.evaluate();
    }

    public Expression left() {
        return this.left;
    }

    public Expression right() {
        return this.right;
    }
}
