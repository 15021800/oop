package sortAccount;

public class DemoClient {
    public static void main(String[] args) {
        BankAccount a1 = new NickleNDime(1000);
        BankAccount a2 = new Gambler(20000);
        BankAccount a3 = new FlatFee(11000);

        a1.deposit(1000);
        a2.withdraw(3000);
        a2.deposit(5000);

        BankAccount[] accounts = {a1, a2, a3};

        BalanceAscending compare = new BalanceAscending();
        InsertionSort.sort(accounts, compare);
        for (BankAccount account : accounts) {
            System.out.print(account.getBalance() + " ");
        }
        System.out.print("\n");

        BalanceDescending compare2 = new BalanceDescending();
        InsertionSort.sort(accounts, compare2);
        for (BankAccount account : accounts) {
            System.out.print(account.getBalance() + " ");
        }
        System.out.print("\n");

        TransactionCountDescending compare3 = new TransactionCountDescending();
        InsertionSort.sort(accounts, compare3);
        for (BankAccount account : accounts) {
            System.out.print(account.getNumberOfTransactions() + " ");
        }
        System.out.print("\n");
    }
}
