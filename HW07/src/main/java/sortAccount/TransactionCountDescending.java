package sortAccount;

public class TransactionCountDescending implements MyComparator {
    public boolean less(BankAccount a1, BankAccount a2) {
        return (a1.getNumberOfTransactions() > a2.getNumberOfTransactions());
    }
}
