package sortAccount;

abstract class BankAccount {
    private int balance;
    private int numberOfTransactions = 0;

    BankAccount(int balance) {
        this.balance = balance;
    }

    public boolean deposit(int money) {
        ++numberOfTransactions;

        if (money < 0) return false;
        balance += money;
        return true;
    }

    public boolean withdraw(int money) {
        ++numberOfTransactions;

        if ((money > balance) || (money < 0)) return false;
        balance -= money;
        return true;
    }

    abstract public int endMonthCharge();

    public void endMonth() {
        int charge = endMonthCharge();

        balance -= charge;
        System.out.printf("%d %d %d\n", balance, numberOfTransactions, charge);
        numberOfTransactions = 0;
    }

    int getBalance() {
        return balance;
    }

    int getNumberOfTransactions() {
        return numberOfTransactions;
    }
}
