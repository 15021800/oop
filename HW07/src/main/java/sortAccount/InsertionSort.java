package sortAccount;

public class InsertionSort {
    public static void sort(BankAccount[] accounts, MyComparator compare) {
        for (int i = 1; i < accounts.length; ++i) {
            int j = i;
            while ((j > 0) && (compare.less(accounts[j], accounts[j-1]))) {
                BankAccount tmp;
                tmp = accounts[j];
                accounts[j] = accounts[j-1];
                accounts[j-1] = tmp;

                --j;
            }
        }
    }
}
