package sortAccount;

public class FlatFee extends BankAccount {
    private int fee = 10000;

    FlatFee(int balance) {
        super(balance);
    }

    @Override
    public int endMonthCharge() {
        return fee;
    }
}
