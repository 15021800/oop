package sortAccount;

public class NickleNDime extends BankAccount {
    private int fee = 0;

    NickleNDime(int balance) {
        super(balance);
    }

    @Override
    public boolean withdraw(int money) {
        if (super.withdraw(money)) {
            fee += 2000;
            return true;
        }

        return false;
    }

    public int endMonthCharge() {
        return fee;
    }
}
