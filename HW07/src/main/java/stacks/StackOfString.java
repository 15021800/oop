package stacks;

public interface StackOfString {
    String pop();
    void push(String s);
}
