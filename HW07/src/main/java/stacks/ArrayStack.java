package stacks;

public class ArrayStack implements StackOfString {
    private String[] Stack;
    private int size = 0;

    public ArrayStack(int capacity) {
        Stack = new String[capacity];
    }

    public String pop() {
        String tmp = Stack[size];
        Stack[size] = null;
        --size;

        return tmp;
    }

    public void push(String s) {
        ++size;
        Stack[size] = s;
    }

    public int getSize() {
        return size;
    }
}
