package stacks;

public class NodeStack implements StackOfString {
    private Node head = null;
    private int size = 0;

    public int getSize() {
        return size;
    }

    private class Node {
        Node next = null;
        String value;

        Node(String s) {
            value = s;
        }
    }

    public String pop() {
        String tmp = head.value;
        head = head.next;
        --size;

        return tmp;
    }

    public void push(String s) {
        Node newNode = new Node(s);
        ++size;

        newNode.next = head;
        head = newNode;
    }
}
