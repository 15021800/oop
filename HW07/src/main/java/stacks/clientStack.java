package stacks;

public class clientStack {
    public static void main(String[] args) {
        StackOfString stack = new NodeStack();
        stack.push("Q");
        stack.push("U");
        stack.push("A");
        stack.push("N");
        stack.push("G");

        System.out.print(stack.pop() + " " + stack.pop() + " " + stack.pop() + " " + stack.pop() + " " + stack.pop() + "\n");

        StackOfString stack2 = new ArrayStack(6);
        stack2.push("Q");
        stack2.push("U");
        stack2.push("A");
        stack2.push("N");
        stack2.push("G");

        System.out.print(stack2.pop() + " " + stack2.pop() + " " + stack2.pop() + " " + stack2.pop() + " " + stack2.pop() + "\n");

    }
}
