package sort;

public class InsertionSort<T extends Comparable> {
    public void sort(T[] arr, MyComparator<T> compare) {
        for (int i = 1; i < arr.length; ++i) {
            int j = i;
            while ((j > 0) && (compare.less(arr[j], arr[j-1]))) {
                T tmp;
                tmp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = tmp;

                --j;
            }
        }
    }
}
