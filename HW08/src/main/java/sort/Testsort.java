package sort;

/**
 * Created by MyPC on 07/11/2016.
 */
public class Testsort {
    public static void main(String[] args) {
        Integer[] arr = {5, 3, 2, 1, -1};

        MyComparator compare = new MyComparator() {
            public boolean less(Comparable a1, Comparable a2) {
                return (a1.compareTo(a2) < 0);
            }
        };

        InsertionSort<Integer> sort = new InsertionSort<Integer>();

        sort.sort(arr, compare);

        for (Integer i:arr) {
            System.out.println(i);
        }
    }
}
