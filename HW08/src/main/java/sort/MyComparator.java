package sort;

public interface MyComparator<T extends Comparable> {
    boolean less(T a1, T a2);
}
