package stack;

public interface Stack<T> extends Iterable<T> {
    T pop();
    void push(T value);
}
