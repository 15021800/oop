package stack;

public class ArrayStack<T> implements Stack<T> {
    private T[] Stack;
    private int size = 0;

    public ArrayStack(int capacity) {
        Stack = (T[]) new Object[capacity];
    }

    public T pop() {
        T tmp = Stack[size];
        Stack[size] = null;
        --size;

        return tmp;
    }

    public void push(T value) {
        ++size;
        Stack[size] = value;
    }

    public int getSize() {
        return size;
    }

    public Iterator<T> interator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<T> {
        private int i = size;

        public boolean hasNext() {
            return i > 0;
        }

        public T next() {
            return Stack[i--];
        }
    }
}
