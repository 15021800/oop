package stack;

public class NodeStack<T> implements Stack<T> {
    private Node head = null;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public Iterator<T> interator() {
        return new IteratorList();
    }

    private class Node {
        Node next = null;
        T value;

        Node(T value) {
            this.value = value;
        }
    }

    public T pop() {
        T tmp = head.value;
        head = head.next;
        --size;

        return tmp;
    }

    public void push(T value) {
        Node newNode = new Node(value);
        ++size;

        newNode.next = head;
        head = newNode;
    }

    private class IteratorList implements Iterator<T> {
        private Node current = head;

        public boolean hasNext() {
            return current != null;
        }

        public T next() {
            T item = current.value;
            current = current.next;
            return item;
        }
    }
}
