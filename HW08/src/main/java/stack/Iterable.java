package stack;

/**
 * Created by MyPC on 08/11/2016.
 */
public interface Iterable<Item> {
    Iterator<Item> interator();
}
