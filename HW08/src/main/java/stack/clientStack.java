package stack;

public class clientStack {
    public static void main(String[] args) {
        NodeStack<String> stack = new NodeStack<String>();
        stack.push("Q");
        stack.push("U");
        stack.push("A");
        stack.push("N");
        stack.push("G");

        Iterator<String> i = stack.interator();
        while (i.hasNext()) {
            String s = i.next();
            System.out.print(s + " ");
        }
        System.out.print("\n");

        Stack<Character> stack2 = new ArrayStack<Character>(6);
        stack2.push('Q');
        stack2.push('U');
        stack2.push('A');
        stack2.push('N');
        stack2.push('G');

        Iterator<Character> j = stack2.interator();
        while (j.hasNext()) {
            Character c = j.next();
            System.out.print(c + " ");
        }
        System.out.print("\n");
    }
}
