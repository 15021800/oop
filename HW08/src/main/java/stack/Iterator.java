package stack;

/**
 * Created by MyPC on 08/11/2016.
 */
public interface Iterator<Item> {
    boolean hasNext();
    Item next();
}
