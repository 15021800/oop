/**
 * Created by Administrator on 9/22/2016.
 */

public class DateAndTime {
    private int second; // 0 - 59
    private int month; // 1-12
    private int day;   // 1-31 based on month
    private int year;  // any year

    public DateAndTime(int theMonth, int theDay, int theYear, int h, int m) {
        this(theMonth, theDay, theYear, h, m, 0);
    }

    public DateAndTime(int theMonth, int theDay, int theYear, int h) {
        this(theMonth, theDay, theYear, h, 0, 0);
    }

    public DateAndTime(int theMonth, int theDay, int theYear) {
        this(theMonth, theDay, theYear, 0, 0, 0);
    }

    public DateAndTime(int theMonth, int theDay, int theYear, int h, int m, int s) {
        year = checkYear( theYear ); // could validate year
        month = checkMonth( theMonth ); // validate month
        day = checkDay( theDay ); // validate day

        setTime(h, m, s);
    }

    public void setTime( int h, int m, int s )
    {
        setHour( h );   // set the hour
        setMinute( m ); // set the minute
        setSecond( s ); // set the second
    } // end method setTime

    // validate and set hour
    public void setHour( int h )
    {
        if ((h < 0) || (h >= 24)) {
            System.out.printf("Invalid value (%d) set to 0\n", h);
            h = 0;
        }

        second = h*3600 + getMinute()*60 + getSecond();
    } // end method setHour

    // validate and set minute
    public void setMinute( int m )
    {
        if ((m < 0) || (m >= 60)) {
            System.out.printf("Invalid value (%d) set to 0\n", m);
            m = 0;
        }

        second = getHour()*3600 + m*60 + getSecond();
    } // end method setMinute

    // validate and set second
    public void setSecond( int s )
    {
        if ((s < 0) || (s >= 60)) {
            System.out.printf("Invalid value (%d) set to 0\n", s);
            s = 0;
        }

        second = getHour()*3600 + getMinute()*60 + s;
    } // end method setSecond

    // Get Methods
    // get hour value
    public int getHour()
    {
        return second / 3600;
    } // end method getHour

    // get minute value
    public int getMinute()
    {
        int tmp = second % 3600;
        return tmp / 60;
    } // end method getMinute

    // get second value
    public int getSecond()
    {
        int tmp = second % 3600;
        tmp = tmp % 60;
        return tmp;
    } // end method getSecond

    public void tick() {
        second = (second + 1) % (3600*24);
    }

    public void incrementMinute() {
        second = (second + 60) % (3600*24);
    }

    public void incrementHour() {
        second = (second + 3600);

        if (second > (3600*24)) nexTDay();
        second %= (3600*24);
    }

    // convert to String in universal-time format (HH:MM:SS)
    public String toUniversalString()
    {
        return String.format(
                "%d/%d/%d \n %02d:%02d:%02d", month, day, year, getHour(), getMinute(), getSecond() );
    } // end method toUniversalString

    // convert to String in standard-time format (H:MM:SS AM or PM)
    public String toString()
    {
        return String.format( "%d/%d/%d \n%d:%02d:%02d %s",
                 month, day, year, ( (getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12 ),
                getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ) );
    } // end method toString

    // utility method to confirm proper month value
    private int checkMonth( int testMonth )
    {
        if ( testMonth > 0 && testMonth <= 12 ) // validate month
            return testMonth;
        else // month is invalid
        {
            System.out.printf(
                    "Invalid month (%d) set to 1.\n", testMonth );
            return 1; // maintain object in consistent state
        } // end else
    } // end method checkMonth

    // utility method to confirm proper day value based on month and year
    private int checkDay( int testDay )
    {
        int daysPerMonth[] =
                { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        // check if day in range for month
        if ( testDay > 0 && testDay <= daysPerMonth[ month ] )
            return testDay;

        // check for leap year
        if ( month == 2 && testDay == 29 && ( year % 400 == 0 ||
                ( year % 4 == 0 && year % 100 != 0 ) ) )
            return testDay;

        System.out.printf( "Invalid day (%d) set to 1.\n", testDay );
        return 1;  // maintain object in consistent state
    } // end method checkDay

    private int checkYear(int testYear) {
        if (testYear < 1) {
            System.out.printf("Invalid year (%d) set to 1.\n", testYear);
            return 1;
        }

        return testYear;
    }

    public void nexTDay() {
        int daysPerMonth[] =
                { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        if ( month == 2 && ( year % 400 == 0 ||
                ( year % 4 == 0 && year % 100 != 0 ) ) ) daysPerMonth[2] = 29;

        ++day;
        if (day > daysPerMonth[month]) {
            day = 1;
            ++month;
        }

        if (month > 12) {
            month = 1;
            ++year;
        }
    }
}
