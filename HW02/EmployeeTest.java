/**
 * Created by Administrator on 9/21/2016.
 */
public class EmployeeTest {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Nguyen", "Quang", 800);
        Employee employee2 = new Employee("Illumi", "Neo", 13388.4);

        System.out.printf("Employee %s %s earn %.2f each year\n\n",
                employee1.getFirstName(), employee1.getLastName(), employee1.getMonthlySalary() * 12);

        System.out.printf("Employee %s %s earn %.2f each year\n\n",
                employee2.getFirstName(), employee2.getLastName(), employee2.getMonthlySalary() * 12);

        employee1.setMonthlySalary(employee1.getMonthlySalary() + employee1.getMonthlySalary()*0.1);
        employee2.setMonthlySalary(employee2.getMonthlySalary() + employee2.getMonthlySalary()*0.1);

        System.out.printf("Employee %s %s earn %.2f each year\n\n",
                employee1.getFirstName(), employee1.getLastName(), employee1.getMonthlySalary() * 12);

        System.out.printf("Employee %s %s earn %.2f each year\n\n",
                employee2.getFirstName(), employee2.getLastName(), employee2.getMonthlySalary() * 12);
    }
}
