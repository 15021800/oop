/**
 * Created by Administrator on 9/21/2016.
 */
public class SavingsAccountTest {
    public static void main(String[] args) {
        SavingsAccount saver1 = new SavingsAccount(2000.00);
        SavingsAccount saver2 = new SavingsAccount(3000.00);

        SavingsAccount.modifyInterestRate(0.04);
        saver1.caculateMonthlyInterest();
        saver2.caculateMonthlyInterest();
        System.out.printf("Saver1 balance: %.2f\n", saver1.getSavingsBalance());
        System.out.printf("Saver2 balance: %.2f\n\n", saver2.getSavingsBalance());

        SavingsAccount.modifyInterestRate(0.05);
        saver1.caculateMonthlyInterest();
        saver2.caculateMonthlyInterest();
        System.out.printf("Saver1 balance: %.2f\n", saver1.getSavingsBalance());
        System.out.printf("Saver2 balance: %.2f\n\n", saver2.getSavingsBalance());
    }
}
