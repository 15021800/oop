import java.io.File;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 9/21/2016.
 */
public class Task5 {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(new File("C:\\Users\\Administrator\\Downloads\\Test.txt"));

            input.useDelimiter(Pattern.compile("[^a-zA-z]"));
            String n;
            while (input.hasNext()) {
                input.skip("[^a-zA-Z]*");
                n = input.next();
                System.out.printf("%s\n", n);
            }
        } catch (java.io.FileNotFoundException error) {
            System.out.print("File not found!");
        }
    }
}
