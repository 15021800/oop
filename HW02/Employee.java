import static java.lang.Double.max;

/**
 * Created by Administrator on 9/21/2016.
 */
public class Employee {
    private String firstName, lastName;
    private double monthlySalary;

    Employee (String firstName, String lastName, double monthlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.monthlySalary = max(0, monthlySalary);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = max(0, monthlySalary);
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


}
