/**
 * Created by Administrator on 9/21/2016.
 */
public class SavingsAccount {
    private double savingsBalance;
    static double annualInterestRate;

    SavingsAccount(double savingsBalance) {
        setSavingsBalance(savingsBalance);
    }

    public void caculateMonthlyInterest() {
        savingsBalance += savingsBalance*(annualInterestRate / 12);
    }

    public static void modifyInterestRate(double rate) {
        annualInterestRate = rate;
    }

    public void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    public double getSavingsBalance() {
        return savingsBalance;
    }
}
