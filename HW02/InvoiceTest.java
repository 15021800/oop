/**
 * Created by Administrator on 9/21/2016.
 */
public class InvoiceTest {
    public static void main(String[] args) {
        Invoice Item1 = new Invoice("001", "pencil", 10, 9.5);
        Invoice Item2 = new Invoice("002", "rule", 9, 20.5);

        System.out.printf("Number: %s, Description: %s, Quantity: %d, Price: %.2f.\nTotal price: %.2f\n\n",
                Item1.getPartNumber(), Item1.getPartDescription(), Item1.getQuantity(), Item1.getPrice(), Item1.getInvoiceAmount());

        System.out.printf("Number: %s, Description: %s, Quantity: %d, Price: %.2f.\nTotal price: %.2f\n\n",
                Item2.getPartNumber(), Item2.getPartDescription(), Item2.getQuantity(), Item2.getPrice(), Item2.getInvoiceAmount());

        Item2.setPartNumber("003");
        Item2.setPartDescription("Alien things");
        Item2.setQuantity(-1);
        Item2.setPrice(-999);

        System.out.printf("Number: %s, Description: %s, Quantity: %d, Price: %.2f.\nTotal price: %.2f\n\n",
                Item2.getPartNumber(), Item2.getPartDescription(), Item2.getQuantity(), Item2.getPrice(), Item2.getInvoiceAmount());
    }
}
