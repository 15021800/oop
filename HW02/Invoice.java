/**
 * Created by Administrator on 9/21/2016.
 */

import static java.lang.Math.max;

public class Invoice {
    private String partNumber, partDescription;
    private int quantity;
    private double price;

    Invoice (String partNumber, String partDescription, int quantity, double price) {
        this.partNumber = partNumber;
        this.partDescription = partDescription;
        this.price = max(0, price);
        this.quantity = max(0, quantity);
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public void setPrice(double price) {
        this.price = max(0, price);
    }

    public void setQuantity(int quantity) {
        this.quantity = max(0, quantity);
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public String getPartNumber() {
        return partNumber;
    }

    double getInvoiceAmount() {
        return price*quantity;
    }
}
