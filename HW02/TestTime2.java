/**
 * Created by Administrator on 9/21/2016.
 */

public class TestTime2 {
    public static void main(String[] args) {
        Time2 time = new Time2(22, 58, 59);

        time.incrementHour();
        String s = time.toString();
        System.out.print(s + "\n");

        time.incrementMinute();
        s = time.toString();
        System.out.print(s + "\n");

        time.tick();
        s = time.toString();
        System.out.print(s + "\n");

        Time2 time2 = new Time2(24, 61, -1);
    }
}
