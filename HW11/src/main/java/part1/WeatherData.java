package part1;

import java.util.ArrayList;

/**
 * Created by MyPC on 08/12/2016.
 */
public class WeatherData implements Subject {
    private float temperature, humidity, pressure;
    private ArrayList<Observer> observers;

    WeatherData() {
        observers = new ArrayList<Observer>();
    }

    void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;

        this.measurementsChanged();
    }

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        int i = observers.indexOf(observer);

        if (i >= 0) {
            observers.remove(i);
        }
    }

    public void notifyObserver() {
        for (Observer observer : observers) {
            observer.update(temperature, humidity, pressure);
        }
    }

    public float getTemperature() {

        return temperature;
    }

    public float getHumidity() {

        return humidity;
    }

    public float getPressure() {

        return pressure;
    }

    private void measurementsChanged() {
        notifyObserver();
    }
}
