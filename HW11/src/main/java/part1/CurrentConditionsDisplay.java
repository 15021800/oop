package part1;

/**
 * Created by MyPC on 08/12/2016.
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private float temperature, humidity;

    CurrentConditionsDisplay(WeatherData weatherData) {
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Current conditions: " + temperature + "F degrees and " + humidity + "% humidity");
    }

    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.display();
    }
}
