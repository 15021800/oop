package part1;

/**
 * Created by MyPC on 08/12/2016.
 */
public interface Observer {
    void update(float temperature, float humidity, float pressure);
}
