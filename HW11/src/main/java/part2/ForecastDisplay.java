package part2;

/**
 * Created by MyPC on 08/12/2016.
 */
public class ForecastDisplay implements Observer, DisplayElement {
    ForecastDisplay(WeatherData weatherData) {
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("I don't know how to forecast the weather!");
    }

    public void update(float temperature, float humidity, float pressure) {
        display();
    }
}
