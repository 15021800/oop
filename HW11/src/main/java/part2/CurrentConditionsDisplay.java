package part2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by MyPC on 08/12/2016.
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private WeatherData weatherData;

    CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Current conditions: " + weatherData.getTemperature() + "F degrees and " + weatherData.getHumidity() + "% humidity");
    }

    public void update(Observable o, Object arg) {
        display();
    }
}
