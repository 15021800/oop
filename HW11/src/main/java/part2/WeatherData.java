package part2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by MyPC on 08/12/2016.
 */
public class WeatherData extends Observable {
    private float temperature, humidity, pressure;

    void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;

        this.measurementsChanged();
    }

    public void registerObserver(Observer observer) {
        super.addObserver(observer);
    }

    public void removeObserver(Observer observer) {
        super.deleteObserver(observer);
    }

    public void notifyObservers() {
        super.notifyObservers();
    }

    public float getTemperature() {

        return temperature;
    }

    public float getHumidity() {

        return humidity;
    }

    public float getPressure() {

        return pressure;
    }

    private void measurementsChanged() {
        notifyObservers();
    }
}
