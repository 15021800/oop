package part2;

import java.util.ArrayList;

/**
 * Created by MyPC on 08/12/2016.
 */
public class StatisticsDisplay implements Observer, DisplayElement {
    private float minTemp = 1000000, maxTemp = -1000000;
    private ArrayList<Float> allTemps = new ArrayList<Float>();

    StatisticsDisplay(WeatherData weatherData) {
        weatherData.registerObserver(this);
    }

    private float getAvgTemp() {
        float avgTemp = 0;
        for (float f: allTemps) {
            avgTemp += f;
        }

        avgTemp = avgTemp / allTemps.size();

        return avgTemp;
    }

    public void display() {
        float avgTemp = this.getAvgTemp();

        System.out.println("Avg/Max/Min temperature = " + avgTemp + "/" + maxTemp + "/" + minTemp);
    }

    public void update(float temperature, float humidity, float pressure) {
        minTemp = Math.min(temperature, minTemp);
        maxTemp = Math.max(temperature, maxTemp);
        allTemps.add(temperature);

        display();
    }
}
